const { user_game } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game.findAll();

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await roles.create({
                name: req.body.name
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await roles.update({
                name: req.body.name
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await roles.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    }
} 
