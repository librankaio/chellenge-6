const { user_game_biodata } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game_biodata.findAll();

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game_biodata.create({
                nickname: req.body.nickname,
                hobby: req.body.hobby,
                jenis_kelamin: req.body.jenis_kelamin
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game_biodata.update({
                nickname: req.body.nickname,
                hobby: req.body.hobby,
                jenis_kelamin: req.body.jenis_kelamin
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game_biodata.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    }
} 
