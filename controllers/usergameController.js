const { user_game, user_game_history } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game.findAll({
                include: [
                    { model: user_game_history, as: 'user_game_history' }
                ]
            });
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game.create({
                username: req.body.username,
                password: req.body.password
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game.update({
                username: req.body.username,
                password: req.body.password
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    }
} 
