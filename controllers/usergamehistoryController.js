const { user_game_history } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game_history.findAll();

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game_history.create({
                nickname: req.body.nickname,
                time: req.body.time,
                score: req.body.score
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game_history.update({
                nickname: req.body.nickname,
                time: req.body.time,
                score: req.body.score
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game_history.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message: "Fatal Error!"
            })
        }
    }
} 
