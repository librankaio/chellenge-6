const express = require('express')
const router = express.Router()
const roleController = require('./controllers/roleController');
const usergameController = require('./controllers/usergameController');
const usergamebiodataController = require('./controllers/usergamebiodataController');
const usergamehistoryController = require('./controllers/usergamehistoryController');

router.get('/role/list', roleController.list)
router.post('/role/create', roleController.create)
router.put('/role/update', roleController.update)
router.delete('/role/destroy', roleController.destroy)

router.get('/user_game', usergameController.list)
router.post('/user_game/create', usergameController.create)
router.put('/user_game/update', usergameController.update)
router.delete('/user_game/destroy', usergameController.destroy)

router.get('/user_game_biodata', usergamebiodataController.list)
router.post('/user_game_biodata/create', usergamebiodataController.create)
router.put('/user_game_biodata/update', usergamebiodataController.update)
router.delete('/user_game_biodata/destroy', usergamebiodataController.destroy)

router.get('/user_game_history', usergamehistoryController.list)
router.post('/user_game_history/create', usergamehistoryController.create)
router.put('/user_game_history/update', usergamehistoryController.update)
router.delete('/user_game_history/destroy', usergamehistoryController.destroy)

module.exports = router